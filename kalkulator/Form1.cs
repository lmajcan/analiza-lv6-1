﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace kalkulator
{
    public partial class Form1 : Form
    {
        Double value = 0;
        String operation = "";
        bool operation_pressed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            if ((rezultat.Text == "0")||(operation_pressed))
            {
                rezultat.Clear();
            }
            operation_pressed = false;
            Button b = (Button)sender;
            if (b.Text == ",")
            {
                if (!rezultat.Text.Contains(","))
                {
                    rezultat.Text = rezultat.Text + b.Text;
                }

            }
            else
                rezultat.Text = rezultat.Text + b.Text;
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            rezultat.Text = "0";
        }

        private void operator_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (value != 0)
            {
                jednako.PerformClick();
                operation_pressed = true;
                label1.Text = value + " " + operation;

            }
            else
            {

            }
                operation = b.Text;
            value = Double.Parse(rezultat.Text);
            operation_pressed = true;
            label1.Text = value + " " + operation;
        }

        private void jednako_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            switch (operation)
            {
                case "+":
                    rezultat.Text=(value + Double.Parse(rezultat.Text)).ToString();
                    break;
                case "*":
                    rezultat.Text = (value * Double.Parse(rezultat.Text)).ToString();
                    break;
                case "/":
                    rezultat.Text = (value / Double.Parse(rezultat.Text)).ToString();
                    break;
                case "-":
                    rezultat.Text = (value - Double.Parse(rezultat.Text)).ToString();
                    break;
                default:
                    break;    
            }

            value = Double.Parse(rezultat.Text);
            operation = "";

        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            rezultat.Clear();
            value = 0;
            label1.Text = "";
        }

        private void button_sin_Click(object sender, EventArgs e)
        {
            rezultat.Text = (Math.Sin(Double.Parse(rezultat.Text))).ToString();
        }

        private void button_cos_Click(object sender, EventArgs e)
        {
            rezultat.Text = (Math.Cos(Double.Parse(rezultat.Text))).ToString();
        }

        private void button_tg_Click(object sender, EventArgs e)
        {
            rezultat.Text = (Math.Tan(Double.Parse(rezultat.Text))).ToString();
        }

        private void button_sqrt_Click(object sender, EventArgs e)
        {
            rezultat.Text = (Math.Sqrt(Double.Parse(rezultat.Text))).ToString();
        }

        private void button_ln_Click(object sender, EventArgs e)
        {
            rezultat.Text = (Math.Log(Double.Parse(rezultat.Text))).ToString();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            switch (e.KeyChar.ToString())
            {
                case "0":
                    button0.PerformClick();
                    break;
                case "1":
                    button1.PerformClick();
                    break;
                case "2":
                    button2.PerformClick();
                    break;
                case "3":
                    button3.PerformClick();
                    break;
                case "4":
                    button4.PerformClick();
                    break;
                case "5":
                    button5.PerformClick();
                    break;
                case "6":
                    button6.PerformClick();
                    break;
                case "7":
                    button7.PerformClick();
                    break;
                case "8":
                    button8.PerformClick();
                    break;
                case "9":
                    button9.PerformClick();
                    break;
                case "+":
                    plus.PerformClick();
                    break;
                case "-":
                    minus.PerformClick();
                    break;
                case "*":
                    puta.PerformClick();
                    break;
                case "/":
                    podjeljeno.PerformClick();
                    break;
                case "=":
                    jednako.PerformClick();
                    break;
                default:
                    break;





            }
        }

   


        
    }
}
